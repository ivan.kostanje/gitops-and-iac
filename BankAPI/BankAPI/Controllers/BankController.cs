﻿using Microsoft.AspNetCore.Mvc;
using BankAPI.Models;

namespace BankAPI.Controllers;

[ApiController]
[Route("api/transmit")]
public class BankController : ControllerBase
{
    [HttpPost]
    public IActionResult Post([FromBody] TransferRequest request)
    {
        if (request.Amount < 0)
        {
            return BadRequest("Amount must be positive");
        }
        if (string.IsNullOrEmpty(request.IBAN))
        {
            return BadRequest("IBAN must be provided");
        }
        return Ok();
    }
}