# GitOps and IaC

## Group Members
- Badertscher Janis
- Bellwald Nicolas
- Kostanjevec Ivan

## Introduction
Our project is to discover GitOps and Infrastructure as Code (IaC) with the help of Azure and Terraform. 
By creating a web application with various services, we intend to discover the benefits and drawbacks of GitOps and IaC.
In order to do that, we decided to use following technologies:
- Terraform
- Azure
- GitLab
- Docker

### Structure of our repository

You will find various folders in our repository. These can be divided into applications and documentation:

Applications (code, dockerfile)
- Bank API
- accounting
- ahvAPI
- safetynet
- safetynetfront

Documentation (different ReadMe files about specific topic)
- documentation

The documentation folder also contains the following subfolders with the corresponding ReadMe files:
- [gitops](documentation/gitops/README.md): Information about GitOps and our implementation
- [setup](documentation/setup/README.md): Explanation how to set up the project to recreate it
- [terraform](documentation/terraform/README.md): Information about the installation of Terraform and some general concepts

## Technologies
### Terraform

Terraform, which was developed by Hashicorp, is a tool that is used for the configuration of Infrastructure as code (IaC). 
This means that developers can use Terraform to describe infrastructure resources (virtual machines, cloud services, databases, etc.) as code. 
This code is then used by Terraform to create the described infrastructure on a provider such as Microsoft Azure, Amazon, etc. 
The configuration language of Terraform is called "HashiCorp Configuration Language" (HCL).

In our project, we use Terraform to create the infrastructure on Microsoft Azure and also provide our resources (container groups, containers, databases, servers, etc.) on Microsoft Azure. 
We also use Terraform in combination with GitLab and the associated continuous integration/continuous deployment to automate any infrastructure changes via the pipelines.

### Azure

Azure is a cloud computing service created by Microsoft for building, testing, deploying, and managing applications and
services through Microsoft-managed data centers. It provides software as a service (SaaS), platform as a service (PaaS)
and infrastructure as a service (IaaS) and supports many different programming languages, tools, and frameworks,
including both Microsoft-specific and third-party software and systems.

We use Microsoft Azure in our project as a provider, where we deploy our resources (container group, containers with the applications, databases, etc.).

### GitLab

GitLab is a web-based DevOps platform that offers various functions such as version control, continuous integration/continuous deployment (CI/CD), code review, etc.
GitLab enables development teams to host, manage and review their software projects in a single environment. 
As GitLab is an open source solution, this means that GitLab is free to use.

### Docker

Docker is a set of platform as a service (PaaS) products that use OS-level virtualization to deliver software in
packages called containers. Containers are isolated from one another and bundle their own software, libraries and
configuration files; they can communicate with each other through well-defined channels. All containers are run by a
single operating system kernel and are thus more lightweight than virtual machines.

In our project, we use Docker to provide our applications as Docker containers.


## Setup
To be able to replicate the project and make it run on your own Azure subscription, you will need to follow our
[setup guide](./documentation/setup/README.md). This guide will walk you through the entire process.

## Alternatives

### OpenTofu

[OpenTofu](https://opentofu.org/) is the most obvious alternative to Terraform as it is a direct fork of Terraform,
this is because Terraform changed its license.
OpenTofu on the other hand is open source and can also be easily used on platforms like GitLab.
GitLab even recommends the use OpenTofu instead of Terraform. As it is a Terraform fork, it functions exactly the same.
You can use the same commands, just instead of `terraform` you always use `tofu`.

### Vendor specific solutions

Many different cloud providers have their own tools to manage their cloud infrastructure. Some examples are:

- [AWS CloudFormation](https://aws.amazon.com/de/cloudformation/)
- [Azure Resource Manager](https://azure.microsoft.com/en-us/get-started/azure-portal/resource-manager/)
- [Google Cloud Deployment Manager](https://cloud.google.com/deployment-manager/docs?hl=de)

These tools come with benefits but also drawbacks. They are usually very good when working with their appropriate cloud
platform. But you can't really change the cloud provider when you use a vendor-specific tool. You are locked into
the chosen ecosystem if you don't want to redo everything from the start.

### Pulumi

[Pulumi](https://www.pulumi.com/) sets itself apart from the competition
by providing support for multiple different programming languages.
This includes languages like Python, Java and C#. Its biggest selling point is also its greatest drawback for Git-Ops.
In Git-Ops you usually want a declarative way of creating infrastructure, but Pulumi is more on the imperative side.

### Ansible

[Ansible](https://www.ansible.com/) is the IaC tool from Red Hat. It uses like Terraform a declarative syntax. Its configurations are written as
yaml files. As Terraform, you can use it to manage different platforms like Azure or AWS. It seems like a very robust
tool, but in our opinion, the documentation is a bit harder to understand.