package ch.hesso.safetynet.model;

/**
 * @author Ivan Kostanjevec
 */
public record InsuranceClaim(
        String firstName,
        String lastName,
        int costEstimate,
        String ahvNumber,
        String iban) {
}
